#=======================================================================
# Nginx configuration for LemonLDAP::NG Handler
#=======================================================================
# This file implements the reload virtualhost that permits to reload
# configuration without restarting server.
# You need then to declare this vhost in reloadUrls (in the manager
# interface if this server doesn't host the manager itself):
#
#         KEY       :               VALUE
#   host-or-IP:port :  http://reload.example.com/reload
#
# IMPORTANT:
# To protect applications, see test-nginx.conf template in example files

# Log format
include __CONFDIR__/nginx-lmlog.conf;
#access_log /var/log/nginx/access.log lm_combined;

# Error pages
error_page 403 http://auth.__DNSDOMAIN__/lmerror/403;
error_page 404 http://auth.__DNSDOMAIN__/lmerror/404;
error_page 500 http://auth.__DNSDOMAIN__/lmerror/500;
error_page 502 http://auth.__DNSDOMAIN__/lmerror/502;
error_page 503 http://auth.__DNSDOMAIN__/lmerror/503;

server {
  listen __PORT__;
  listen [::]:__PORT__;
  server_name reload.__DNSDOMAIN__;
  root /var/www/html;

  # Uncomment this if you are running behind a reverse proxy and want
  # LemonLDAP::NG to see the real IP address of the end user
  # Adjust the settings to match the IP address of your reverse proxy
  # and the header containing the original IP address
  # As an alternative, you can use the PROXY protocol
  #
  #set_real_ip_from  127.0.0.1;
  #real_ip_header    X-Forwarded-For;

  location = /reload {
    allow 127.0.0.0/8;
    allow ::1/128;
    deny all;

    # FastCGI configuration
    include /etc/nginx/fastcgi_params;
    fastcgi_pass unix:__FASTCGISOCKDIR__/llng-fastcgi.sock;
    fastcgi_param LLTYPE reload;

    # Or with uWSGI
    #include /etc/nginx/uwsgi_params;
    #uwsgi_pass 127.0.0.1:5000;
    #uwsgi_param LLTYPE reload;
  }

  # Client requests
  location / {
    deny all;
  }
}
